import React from 'react';
import './css.less'
import logo from './google.png'
import app from './app.jpg'
import user from './user.jpg';

const App = ()=>{
    return (
        <div className='font-style'>
            <header>
                <a>Gmail</a>
                <a>Images</a>
                <a><img src={app}/></a>
                <a><img src={user}/></a>
            </header>
            <img className='googleimg' src={logo} alt="Logo" />
            <div>
                <input type='text' className='search'/>
            </div>
            <div className='button-div'>
                <button className='button-style'><a href="">Google Search</a></button>
                <button className='button-style'><a href="">I am Feeling Lucky</a></button>
            </div>
            <p>Google offered in:<a href="">中文(简体)</a> <a href="">English</a></p>
            <footer>
                <p><span><a>Hong Kong</a></span></p>
                <p>
                    <span><a href="">Advertising</a></span>
                    <span><a href="">Business</a></span>
                    <span><a href="">About</a></span>
                    <span><a href="">How Search works</a></span>
                </p>
            </footer>
        </div>
    );
}


export default App;
